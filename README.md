# fabio

[![pipeline status](https://gitlab.esrf.fr/silx/bob/fabio/badges/master/pipeline.svg)](https://gitlab.esrf.fr/silx/bob/fabio/pipelines)

Build binary packages for [fabio](https://github.com/silx-kit/fabio): [Latest build](https://silx.gitlab-pages.esrf.fr/bob/fabio)
